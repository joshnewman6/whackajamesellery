// game.js
let scene = 1;
let knox = 0;

// PRELOAD
const imageUrls = [
    'Scene1.png',
    'Scene2.png',
    'Scene3.png',
    'Scene4.png',
    'hahahahahhahahhaha.gif',
    'james1.png',
    'james2.png',
    'james3.png',
    'james4.png'
  ];
  
  function preloadImages() {
    for (const imageUrl of imageUrls) {
      const img = new Image();
      img.src = imageUrl;
    }
  }
  
  preloadImages();

// USERNAME STUFF
let username = getCookie('username');
if (!username) {
    username = prompt('Enter your name: ');
    setCookie('username', username, 1000);
}

if (username === null) {
    username = prompt('Enter your name: ');
    setCookie('username', username, 1000);
}

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// MARGINS AND DISPLAY SIZE
function checkScreenDimensions() {
    const screenHeight = window.innerHeight;
    const screenWidth = window.innerWidth;
  
    if (screenHeight > 730 && screenWidth < 900) {
      knox = 1;
    }
}

window.addEventListener("load", checkScreenDimensions);
window.addEventListener("resize", checkScreenDimensions);

const gameEl = document.getElementById('game');
const screenMargin = 0.15; // 10% margin
const bottomMargin = 0.2; // 20% margin at the bottom

function setMargins() {
    document.body.style.marginBottom = `${bottomMargin * window.innerHeight}px`;
    document.documentElement.style.marginBottom = `${bottomMargin * window.innerHeight}px`;
}

setMargins();

window.addEventListener('resize', setMargins);

function checkScreenSize() {
    var screenWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    var screenHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
  
    if (screenWidth <= 1200 || screenHeight <= 500) {
      knox = 1;
      console.log("Session ineligible for leaderboard entry")
    }
}
  
window.addEventListener('load', checkScreenSize);
window.addEventListener('resize', checkScreenSize);

function enterFullscreen() {
    const element = document.documentElement; // Get the HTML element

    if (element.requestFullscreen) {
        element.requestFullscreen(); // Standard
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen(); // Firefox
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen(); // Chrome, Safari, and Opera
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen(); // IE/Edge
    }
}

// Function to exit fullscreen mode
function exitFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen(); // Standard
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen(); // Firefox
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen(); // Chrome, Safari, and Opera
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen(); // IE/Edge
    }
}

let fullscreenToggled = false;
document.addEventListener('click', function () {
    if (!fullscreenToggled) {
        enterFullscreen();
        fullscreenToggled = true;
    }
});

//SCORE
function updateScoreCounter() {
    const scoreCounter = document.getElementById('score-counter');
    scoreCounter.textContent = `Score: ${score}`;
}

// FOLLOW MOUSE
document.addEventListener('DOMContentLoaded', () => {
    const fastMouseImage = document.getElementById('fast-mouse');

    fastMouseImage.style.opacity = '0'; 

    document.addEventListener('mousemove', (event) => {
        const mouseX = event.clientX;
        const mouseY = event.clientY;

        const speed = calculateMouseSpeed(event);
        const opacity = speed * 1.2;

        fastMouseImage.style.left = `${mouseX}px`;
        fastMouseImage.style.top = `${mouseY}px`;
        fastMouseImage.style.opacity = opacity.toString();
    });

    function calculateMouseSpeed(event) {
        if (!fastMouseImage.lastMousePosition) {
            fastMouseImage.lastMousePosition = { x: event.clientX, y: event.clientY };
            fastMouseImage.lastTimestamp = Date.now();
            return 0;
        }

        const currentTime = Date.now();
        const deltaTime = currentTime - fastMouseImage.lastTimestamp;

        const deltaX = event.clientX - fastMouseImage.lastMousePosition.x;
        const deltaY = event.clientY - fastMouseImage.lastMousePosition.y;

        const distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        const speed = distance / deltaTime;

        fastMouseImage.lastMousePosition = { x: event.clientX, y: event.clientY };
        fastMouseImage.lastTimestamp = currentTime;

        return speed;
    }
});

const followMouseImage = document.getElementById('follow-mouse');

document.addEventListener('mousemove', (event) => {
    const followMouseImage = document.getElementById('follow-mouse');

    const mouseX = event.clientX;
    const mouseY = event.clientY;
    
    followMouseImage.style.left = `${mouseX}px`;
    followMouseImage.style.top = `${mouseY}px`;
});

document.addEventListener('mousemove', (event) => {
    const fastMouseImage = document.getElementById('fast-mouse');

    const mouseX = event.clientX;
    const mouseY = event.clientY;
    
    fastMouseImage.style.left = `${mouseX}px`;
    fastMouseImage.style.top = `${mouseY}px`;
});

document.addEventListener('DOMContentLoaded', () => {
    const whackerImage = document.getElementById('whacker');
    const whackedImage = document.getElementById('whacked');

    document.addEventListener('mousedown', (event) => {
        if (event.target.tagName === 'BUTTON') {
            // Click originated from a button, skip the logic
            return;
        }

        if (event.button === 0) { // Left mouse button
            const mouseX = event.clientX;
            const mouseY = event.clientY;

            whackerImage.style.left = `${mouseX}px`;
            whackerImage.style.top = `${mouseY}px`;
            whackerImage.style.display = 'block';

            setTimeout(() => {
                whackerImage.style.display = 'none';
                whackedImage.style.left = `${mouseX}px`;
                whackedImage.style.top = `${mouseY}px`;
                whackedImage.style.display = 'block';

                setTimeout(() => {
                    whackedImage.style.display = 'none';
                }, 100);
            }, 100);
        }
    });
});

function sendscore(score) {
    // Check if knox is equal to 0
    if (knox === 0) {
        const username = document.cookie.replace(/(?:(?:^|.*;\s*)username\s*=\s*([^;]*).*$)|^.*$/, "$1");

        // Get the current timestamp in ACST (Australian Central Standard Time)
        const currentTimestamp = new Date().toLocaleString("en-AU", { timeZone: "Australia/Adelaide" });

        const data = `${username};${score};${currentTimestamp}`;
        
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "https://safetychecker.quinquadcraft.org/waie4/", true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    console.log("Successfully sent score to leaderboard server");
                } else {
                    var expirationDate = new Date();
                    expirationDate.setFullYear(expirationDate.getFullYear() + 1);
                    var cookieValue = "score=" + score + "; expires=" + expirationDate.toUTCString() + ";";
                    document.cookie = cookieValue;
                }
            }
        };
        var jsonData = JSON.stringify({ text: data });
        xhr.send(jsonData);
    } else {
    }
}
//ADDITIONAL MOUSE BEHAVIOUR
document.addEventListener('contextmenu', function(event) {
    event.preventDefault();
});

// JAMES.PNG SPAWNING
const jamesImages = ['james1.png', 'james2.png', 'james3.png', 'james4.png'];
const MAX_JAMES_IMAGES = 9999;  // Maximum number of James images

let jamesTimers = []; // Array to store timers for each James image
let score = 0;

function spawnJamesImage() {
    if (jamesTimers.length >= MAX_JAMES_IMAGES) {
        return; 
    }

    const x = getRandomCoordinate(window.innerWidth * screenMargin, window.innerWidth * (1 - screenMargin));
    const y = getRandomCoordinate(window.innerHeight * screenMargin, window.innerHeight * (1 - screenMargin));
    
    const randomCamera = Math.floor(Math.random() * 4) + 1;
	
    const randomImageIndex = randomCamera - 1;
    const jamesImage = document.createElement('img');
    jamesImage.src = jamesImages[randomImageIndex];
    jamesImage.className = 'james-image';
    jamesImage.draggable = false;
    jamesImage.style.left = `${x}px`;
    jamesImage.style.top = `${y}px`;
    jamesImage.addEventListener('click', () => jamesClicked(jamesImage));
    
    gameEl.appendChild(jamesImage);
    
    const jamesTimer = setTimeout(() => {
        endGame();
    }, 10000);
	
    const jamesData = {
        image: jamesImage,
        timer: jamesTimer
    };

    jamesTimers.push(jamesData);
    jamesTimers.push(jamesTimer);
    
    updateJamesVisibility();
}

function getRandomCoordinate(min, max) {
    return Math.random() * (max - min) + min;
}

function updateJamesVisibility() {
    const jamesImageElements = document.querySelectorAll('.james-image');
    jamesImageElements.forEach(image => {
        image.style.display = 'none';
    });

    for (let i = 0; i < jamesTimers.length; i++) {
        const currentCamera = i + 1;
        const visibleImage = document.querySelector(`.james-image[src='${jamesImages[i]}']`);
        if (visibleImage) {
            visibleImage.style.display = 'block';
        }
    }
}

function playRandomSound() {
    const soundNumber = Math.floor(Math.random() * 4) + 1;
    const audioElement = document.getElementById('sound' + soundNumber);
    if (audioElement) {
        audioElement.play();
    }
}

function jamesClicked(clickedImage) {
    score += 1;
    playRandomSound()
    const jamesIndex = jamesTimers.findIndex(data => data.image === clickedImage);
    
    if (jamesIndex !== -1) {
        clearTimeout(jamesTimers[jamesIndex].timer);
        jamesTimers.splice(jamesIndex, 1);
    }
    
    clickedImage.parentNode.removeChild(clickedImage);
    updateScoreCounter();
}

setInterval(() => {
  spawnJamesImage();
  hideJamesOnNewSpawn(currentCamera);
}, 5000);

document.addEventListener("DOMContentLoaded", function() {
    var jamesImages = document.querySelectorAll("img[src$='james1.png'], img[src$='james2.png'], img[src$='james3.png'], img[src$='james4.png']");

    jamesImages.forEach(function(image, index) {
        image.dataset.jamesIndex = index;
        image.addEventListener("click", function() {
            setTimeout(function() {
                jamesClicked(image);
            }, 100);
        });
    });
});


function showOnlyJames4Images() {
    const jamesImageElements = document.querySelectorAll('.james-image');
    jamesImageElements.forEach(image => {
        const imageName = image.getAttribute('src');
        if (imageName === 'james4.png') {
            image.style.display = 'block';
        } else {
            image.style.display = 'none';
        }
    });
}

function showOnlyJames3Images() {
    const jamesImageElements = document.querySelectorAll('.james-image');
    jamesImageElements.forEach(image => {
        const imageName = image.getAttribute('src');
        if (imageName === 'james3.png') {
            image.style.display = 'block';
        } else {
            image.style.display = 'none';
        }
    });
}

function showOnlyJames2Images() {
    const jamesImageElements = document.querySelectorAll('.james-image');
    jamesImageElements.forEach(image => {
        const imageName = image.getAttribute('src');
        if (imageName === 'james2.png') {
            image.style.display = 'block';
        } else {
            image.style.display = 'none';
        }
    });
}

function showOnlyJames1Images() {
    const jamesImageElements = document.querySelectorAll('.james-image');
    jamesImageElements.forEach(image => {
        const imageName = image.getAttribute('src');
        if (imageName === 'james1.png') {
            image.style.display = 'block';
        } else {
            image.style.display = 'none';
        }
    });
}

// JAMES SPAWNING TIME INCREASE AS SCORE INCREASES
function checkAndUpdateScore5() {
  if (score >= 10 && score <= 14) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkAndUpdateScore5, 10000);


function checkAndUpdateScore10() {
  if (score >= 15 && score <= 19) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkAndUpdateScore10, 9000);


function checkAndUpdateScore15() {
  if (score >= 20 && score <= 29) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkAndUpdateScore15, 7000);


function checkAndUpdateScore20() {
  if (score >= 30 && score <= 39) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkAndUpdateScore20, 6000);


function checkAndUpdateScore25() {
  if (score >= 40 && score <= 49) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkAndUpdateScore25, 4500);


function checkAndUpdateScore30() {
  if (score >= 50 && score <= 64) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkAndUpdateScore30, 4000);


function checkAndUpdateScore35() {
  if (score >= 65 && score <= 79) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkAndUpdateScore35, 3000);


function checkAndUpdateScore40() {
  if (score >= 80 && score <= 99) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkAndUpdateScore40, 2800);


function checkAndUpdateScore45() {
  if (score >= 100 && score <= 124) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkAndUpdateScore45, 2000);


function checkAndUpdateScore50() {
  if (score >= 125 && score <= 199) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkAndUpdateScore50, 1500);

function checkScoreAbove100() {
  if (score >= 200) {
    spawnJamesImage();
    hideJamesOnNewSpawn(currentCamera);
  }
}

setInterval(checkScoreAbove100, 1000);

// CAMERA BUTTONS and hideJamesOnNewSpawn
document.addEventListener('keydown', (event) => {
    if (event.key === '1') {
        changeCamera(1);
    } else if (event.key === '2') {
        changeCamera(2);
    } else if (event.key === '3') {
        changeCamera(3);
    } else if (event.key === '4') {
        changeCamera(4);
    }
	  else if (event.key === 'ArrowLeft') {
        currentCamera = (currentCamera - 1 + 4) % 4 || 4;
        changeCamera(currentCamera);
    } else if (event.key === 'ArrowRight') {
        currentCamera = (currentCamera % 4) + 1;
        changeCamera(currentCamera);
    }
      else if (event.key === 'a') {
        currentCamera = (currentCamera - 1 + 4) % 4 || 4;
        changeCamera(currentCamera);
    } else if (event.key === 'd') {
        currentCamera = (currentCamera % 4) + 1;
        changeCamera(currentCamera);
    }
});

let currentCamera = 1;

function enableAllCameraButtons() {
    const cameraButtons = document.querySelectorAll('#cameras button');
    cameraButtons.forEach(button => {
        button.disabled = false;
    });
}

function disableCameraButton(cameraNumber) {
    const cameraButtons = document.querySelectorAll('#cameras button');
    cameraButtons.forEach((button, index) => {
        button.disabled = index + 1 === cameraNumber;
    });
}


function changeCamera(number) {
    currentCamera = number;
	console.log("the current camera is", currentCamera)
    document.body.style.backgroundImage = `url('Scene${number}.png')`;
    disableCameraButton(number);
    if (number === 4) {
        showOnlyJames4Images();
	}
    else if (number === 3) {
        showOnlyJames3Images();
	}
    else if (number === 2) {
        showOnlyJames2Images();
	}
    else if (number === 1) {
        showOnlyJames1Images();
	}
    else {
        console.log("Error: Invalid number in used in changeCamera() function");
    }
}

function hideJamesOnNewSpawn(currentCamera) {
    if (currentCamera === 4) {
        showOnlyJames4Images();
	}
    else if (currentCamera === 3) {
        showOnlyJames3Images();
	}
    else if (currentCamera === 2) {
        showOnlyJames2Images();
	}
    else if (currentCamera === 1) {
        showOnlyJames1Images();
	}
    else {
        console.log("Error: Invalid number in used in hideJamesOnNewSpawn() function");
    }
}

disableCameraButton(currentCamera);

// END OF GAME
function stopSpawning() {
	for (const jamesData of jamesTimers) {
        clearTimeout(jamesData.timer);
    }
	
    jamesTimers = [];
}

function sendScore(score) {
    // Check if knox is equal to 0
    if (knox === 0) {
        const username = document.cookie.replace(/(?:(?:^|.*;\s*)username\s*=\s*([^;]*).*$)|^.*$/, "$1");

        // Get the current timestamp in ACST (Australian Central Standard Time)
        const currentTimestamp = new Date().toLocaleString("en-AU", { timeZone: "Australia/Adelaide" });

        const data = `${username};${score};${currentTimestamp}`;
        
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "https://safetychecker.quinquadcraft.org/waje4/", true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    console.log("Successfully sent score to leaderboard server");
                } else {
                    var expirationDate = new Date();
                    expirationDate.setFullYear(expirationDate.getFullYear() + 1);
                    var cookieValue = "score=" + score + "; expires=" + expirationDate.toUTCString() + ";";
                    document.cookie = cookieValue;
                }
            }
        };
        var jsonData = JSON.stringify({ text: data });
        xhr.send(jsonData);
    } else {
        console.log("Your session was not eligible for a leaderboard entry") 
    }
}

function gameOverMessage() {
	const message = `Game Over! You Whacked ${score} James Ellerys`;
    window.alert(message);
    exitFullscreen()
    knox = 1
    window.location.replace('https://whackajamesellery.joshnewman6.com/4/');
}

function jumpscare() {
  var container = document.createElement('div');
  container.style.position = 'absolute';
  container.style.zIndex = '1001';
  container.style.width = '100%';
  var img = document.createElement('img');
  img.src = 'hahahahahhahahhaha.gif';
  img.style.width = '100%';
  container.appendChild(img);
  document.body.appendChild(container);
}

function playScreamSound() {
  // Create an audio element
  var audio = new Audio('scream.mp3');
  
  // Play the audio
  audio.play();
}

function endGame() {
  stopSpawning();
  sendscore(score);
  playScreamSound()
  jumpscare();
  setTimeout(gameOverMessage, 3000);
}